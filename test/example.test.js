const expect = require('chai').expect
const assert = require('chai').assert
const mylib = require('../src/mylib')

describe("Unit tests for mylib.js", () => {
    before("A before function as per requirements", () => {
        console.log("This is a log from the 'before' function")
    })
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1,2)).equal(3, "1+2 is not 3 for some reason")
    }),
    it("Can substract a random number from a random number + 1", () => {
        let n = Math.floor(Math.random() * 100) + 1;
        expect(mylib.subtract(n, n-1)).equal(1, "Expected 1 and got something different")
    }),
    it("Can multiply two numbers", () => {
        expect(mylib.multiply(2,2)).equal(4, "Expected 4 and got something different")
    }),
    it("5 factorial is greater than 4 factorial", () => {
        expect(mylib.factorial(5)).greaterThan(mylib.factorial(4), "Factorial is f**ed")
    }),
    it("Handles error if division by zero", () => {
        //spent unnecessarily long to figure this out
        assert.throws(() => {
            mylib.divide(5,0)
        }, Error)
    })
    after("A after function as per requirements", () => {
        console.log("This is a log from the 'after' function")
    })
    })