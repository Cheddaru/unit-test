# Unit testing

This is a Node.js exercise on unit testing with Mocha and Chai

# Features

src/mylib.js currently has the following arithmetic functions:

* add
* substract
* multiply
* divide
* factorial