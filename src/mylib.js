module.exports = {
    add: (a,b) => a+b,
    subtract: (a,b) => a-b,
    divide: (dividend, divisor) => 
        {
            if(divisor === 0){
                throw new Error("Cannot divide by zero")
            } else
            return dividend/divisor
        },
    multiply: (a,b) => a*b,
    factorial: function factorial(n) {
        if (n === 0) {
          return 1;
        }
        return n * factorial(n - 1);
      }
    }
