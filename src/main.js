const mylib = require('./mylib');

console.log("Calculate some numbers...");
console.log(mylib.factorial(5))
console.log(mylib.add(1,5))
console.log(mylib.subtract(1,5))
console.log(mylib.divide(5,2))
console.log(mylib.multiply(1,5))
console.log("Done!");